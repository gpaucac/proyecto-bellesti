﻿# Host: localhost  (Version 5.5.5-10.1.33-MariaDB)
# Date: 2021-06-28 16:51:14
# Generator: MySQL-Front 6.1  (Build 1.25)


#
# Structure for table "articulos"
#

DROP TABLE IF EXISTS `articulos`;
CREATE TABLE `articulos` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `fechaCaptura` date DEFAULT NULL,
  PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

#
# Data for table "articulos"
#

INSERT INTO `articulos` VALUES (1,1,1,1,'Alcohol','Para manos',10,2,'2021-06-24'),(2,2,2,1,'Panadol','Forte',20,2,'2021-06-24'),(3,1,1,1,'Amoxicilina','Dolor',10,2,'2021-06-24'),(4,1,1,1,'Ranitidina','Dolor',10,2,'2021-06-24'),(5,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(6,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(7,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(8,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(9,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(10,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(11,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(12,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(13,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(14,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(15,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(16,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(17,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(18,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(19,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(20,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(21,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(22,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(23,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(24,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(25,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(26,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(27,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(28,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(29,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(30,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(31,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(32,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(33,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(34,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(35,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(36,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(37,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(38,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(39,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(40,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(41,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(42,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(43,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(44,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(45,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(46,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(47,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(48,1,1,1,'Panadol','Dolor',10,2,'2021-06-24'),(49,1,1,1,'Panadol','Dolor',10,2,'2021-06-24');

#
# Structure for table "categorias"
#

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nombreCategoria` varchar(150) DEFAULT NULL,
  `fechaCaptura` date DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "categorias"
#

INSERT INTO `categorias` VALUES (1,1,'Limpieza','2021-06-24'),(2,1,'Medicinas','2021-06-24'),(3,3,'Suplementos','2021-06-28');

#
# Structure for table "clientes"
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefono` varchar(200) DEFAULT NULL,
  `rfc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "clientes"
#

INSERT INTO `clientes` VALUES (1,1,'Claudio','Sucapuca','Arequipa','claudio@gmail.com','911110000','11110000'),(2,0,'Pedro','Carrizales',NULL,NULL,NULL,NULL);

#
# Structure for table "imagenes"
#

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE `imagenes` (
  `id_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `ruta` varchar(500) DEFAULT NULL,
  `fechaSubida` date DEFAULT NULL,
  PRIMARY KEY (`id_imagen`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "imagenes"
#

INSERT INTO `imagenes` VALUES (1,1,'coca.jpg','../../archivos/coca.jpg','2021-06-24'),(2,2,'ram.jpg','../../archivos/ram.jpg','2021-06-24');

#
# Structure for table "laboratorios"
#

DROP TABLE IF EXISTS `laboratorios`;
CREATE TABLE `laboratorios` (
  `id_laboratorio` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nombreLaboratorio` varchar(150) DEFAULT NULL,
  `fechaCaptura` date DEFAULT NULL,
  PRIMARY KEY (`id_laboratorio`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

#
# Data for table "laboratorios"
#

INSERT INTO `laboratorios` VALUES (1,1,'Portugal32','2021-06-24'),(3,3,'Teva24','2021-06-28'),(4,3,'Jhonson3','2021-06-28'),(5,3,'Teva1','2021-06-28'),(6,3,'Teva2','2021-06-28'),(7,3,'Teva3','2021-06-28'),(8,3,'Teva4','2021-06-28'),(9,3,'Teva5','2021-06-28'),(10,3,'Teva6','2021-06-28'),(11,3,'Teva7','2021-06-28'),(12,3,'Teva8','2021-06-28'),(13,3,'Teva9','2021-06-28'),(14,3,'Teva10','2021-06-28'),(15,3,'Teva11','2021-06-28'),(16,3,'Teva12','2021-06-28'),(17,3,'Teva13','2021-06-28'),(18,3,'Teva14','2021-06-28'),(19,3,'Teva15','2021-06-28'),(20,3,'Teva16','2021-06-28'),(21,3,'Teva17','2021-06-28'),(22,3,'Teva18','2021-06-28'),(23,3,'Teva19','2021-06-28'),(24,3,'Teva20','2021-06-28'),(25,3,'Teva21','2021-06-28'),(26,3,'Teva22','2021-06-28');

#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` tinytext,
  `fechaCaptura` date DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES (1,'hernan','cama','hernan','e52bf495c6e0f6006d7ec9e4f5bab38aa5263a40','2021-06-23'),(2,'elias','cama','elias','31cc0cbf2a284c9e9ab9489a1b9d091fc8f6c726','2021-06-28'),(3,'admin','admin','admin','31cc0cbf2a284c9e9ab9489a1b9d091fc8f6c726',NULL);

#
# Structure for table "ventas"
#

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "ventas"
#

INSERT INTO `ventas` VALUES (1,1,1,1,2,'2021-06-24'),(1,1,2,1,2,'2021-06-24'),(2,0,1,3,2,'2021-06-28'),(3,1,1,3,2,'2021-06-28'),(4,0,1,3,2,'2021-06-28'),(5,0,2,3,40,'2021-06-28'),(6,0,1,3,20,'2021-06-28');
