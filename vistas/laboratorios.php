<?php 
session_start();
if(isset($_SESSION['usuario'])){

	?>


	<!DOCTYPE html>
	<html>
	<head>
		<title>laboratorios</title>
		<?php require_once "menu.php"; ?>
	</head>
	<body>

		<div class="container">
			<h1>Laboratorios</h1>
			<div class="row">
				<div class="col-sm-12">
					<form id="frmLaboratorios">
						<label>Laboratorio</label>
						<input type="text" class="form-control input-sm" name="laboratorio" id="laboratorio">
						<p></p>
						<span class="btn btn-primary" id="btnAgregaLaboratorio">Agregar</span>
					</form>
				</div>
				<div class="col-sm-12">
					<div id="tablaLaboratorioLoad"></div>
				</div>
			</div>
		</div>

		<!-- Button trigger modal -->

		<!-- Modal -->
		<div class="modal fade" id="actualizaLaboratorio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Actualiza Laboratorios</h4>
					</div>
					<div class="modal-body">
						<form id="frmLaboratorioU">
							<input type="text" hidden="" id="idlaboratorio" name="idlaboratorio">
							<label>Laboratorio</label>
							<input type="text" id="laboratorioU" name="laboratorioU" class="form-control input-sm">
						</form>


					</div>
					<div class="modal-footer">
						<button type="button" id="btnActualizaLaboratorio" class="btn btn-warning" data-dismiss="modal">Guardar</button>

					</div>
				</div>
			</div>
		</div>

	</body>
	</html>
	<script type="text/javascript">
		$(document).ready(function(){

			$('#tablaLaboratorioLoad').load("laboratorios/tablaLaboratorios.php");

			$('#btnAgregaLaboratorio').click(function(){

				vacios=validarFormVacio('frmLaboratorios');

				if(vacios > 0){
					alertify.alert("Debes llenar todos los campos!!");
					return false;
				}

				datos=$('#frmLaboratorios').serialize();
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/laboratorios/agregaLaboratorio.php",
					success:function(r){
						if(r==1){
					//esta linea nos permite limpiar el formulario al insetar un registro
					$('#frmLaboratorios')[0].reset();

					$('#tablaLaboratorioLoad').load("laboratorios/tablaLaboratorios.php");
					alertify.success("Laboratorio agregada con exito :D");
				}else{
					alertify.error("No se pudo agregar laboratorio");
				}
			}
		});
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#btnActualizaLaboratorio').click(function(){

				datos=$('#frmLaboratorioU').serialize();
				$.ajax({
					type:"POST",
					data:datos,
					url:"../procesos/laboratorios/actualizaLaboratorio.php",
					success:function(r){
						if(r==1){
							$('#tablaLaboratorioLoad').load("laboratorios/tablaLaboratorios.php");
							alertify.success("Actualizado con exito :)");
						}else{
							alertify.error("no se pudo actaulizar :(");
						}
					}
				});
			});
		});
	</script>

	<script type="text/javascript">
		function agregaDato(idLaboratorio,laboratorio){
			$('#idlaboratorio').val(idLaboratorio);
			$('#laboratorioU').val(laboratorio);
		}

		function eliminaLaboratorio(idlaboratorio){
			alertify.confirm('¿Desea eliminar esta laboratorio?', function(){ 
				$.ajax({
					type:"POST",
					data:"idlaboratorio=" + idlaboratorio,
					url:"../procesos/laboratorios/eliminarLaboratorio.php",
					success:function(r){
						if(r==1){
							$('#tablaLaboratorioLoad').load("laboratorios/tablaLaboratorios.php");
							alertify.success("Eliminado con exito!!");
						}else{
							alertify.error("No se pudo eliminar :(");
						}
					}
				});
			}, function(){ 
				alertify.error('Cancelo !')
			});
		}
	</script>
	<?php 
}else{
	header("location:../index.php");
}
?>